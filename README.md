# UI/UX Engineer Demonstration of Art

## Assignment

We believe the scope outlined below is likely more than the 4 hours we have asked you to commit to this. We would like you to work through the most important parts first and not work more than 4 hours.

We would like you to create the UI for viewing Service Health, History and Configuration data as a Vue app. Please see the following design artifacts for additional details:

### Needs Statement

The service administrator needs to be able to see their service's status, configuration details, and recent events at a glance.

### API Specification

[admin.openapi.yaml](./admin.openapi.yaml)

**Note:** this spec represents an API that is pending build and deployment. No version of the API is live at this time.

### Low-Fidelity Prototype

![Lo-fi dashboard mockup](./mockup.jpg)

## Project Setup

### Prerequisites

- Node v14 or later

### Install dependencies

```sh
npm install
```

### Compile and hot-reload for development

```sh
npm run serve
```

### Compile and minify for production

```sh
npm run build
```

## Next Steps

On your interview date, be prepared to walk us through your development process and to answer any questions left unanswered by your commit messages.
